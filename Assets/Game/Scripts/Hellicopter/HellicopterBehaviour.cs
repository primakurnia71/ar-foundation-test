﻿using System;
using UnityEngine.UI;
using UnityEngine;

namespace ARTest.Game.Helicopter
{
    public class HellicopterBehaviour : MonoBehaviour
    {

        private Quaternion m_initRotation;
        private Vector3 m_initScale;

        [SerializeField]
        private Button _btnAnimation;
        [SerializeField]
        private Button _btnReset;
        [SerializeField]
        private GameObject m_virtualObject;

        public static Action onObjectClicked;
        public static Action onObjectFlying;

        [SerializeField]
        private float m_rotateSpeedModifier;
        public Quaternion m_rotationY { get; private set; }
        void Start()
        {
            InitButton();
            m_initRotation = transform.rotation;
            m_initScale = m_virtualObject.transform.localScale;

            _btnAnimation?.onClick.AddListener(() =>
            {
                OnButtonClick(_btnAnimation.gameObject, () =>
                {
                    FlyAnimation();
                });
            });

            _btnReset?.onClick.AddListener(() =>
            {
                OnButtonClick(_btnReset.gameObject, () =>
                {
                    ResetTransform();
                });
            });

            LeanTween.scale(gameObject, new Vector3(0.05f, 0.05f, 0.05f), 0.5f);
        }

        void InitButton()
        {
            _btnAnimation = GameObject.Find("BtnAnim").GetComponent<Button>();
            _btnReset = GameObject.Find("BtnReset").GetComponent<Button>();
        }

        void Update()
        {
            RotateObject();
            ScaleObject();
        }

        void RotateObject()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Moved)
                {
                    m_rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * m_rotateSpeedModifier, 0f);
                    transform.rotation = m_rotationY * transform.rotation;
                }
            }
        }

        void ScaleObject()
        {
            if (Input.touchCount == 2)
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                float deltaMagnitudeDiff = touchDeltaMag - prevTouchDeltaMag;
                float pinchAmount = deltaMagnitudeDiff * 0.02f * Time.deltaTime;
                m_virtualObject.transform.localScale += new Vector3(pinchAmount, pinchAmount, pinchAmount);
            }
        }

        private void OnMouseDown()
        {
            if (onObjectClicked != null)
            {
                onObjectClicked();
            }
        }

        public void ResetTransform()
        {
            transform.rotation = m_initRotation;
            m_virtualObject.transform.localScale = m_initScale;
        }

        public void FlyAnimation()
        {
            if (onObjectFlying != null)
            {
                onObjectFlying();
            }
        }

        void OnButtonClick(GameObject button, Action onClick)
        {
            AudioController.Play("Click");
            LeanTween.scale(button, new Vector3(1.1f, 1.1f, 1.1f), 0.1f).setOnComplete(
                    () =>
                    {
                        LeanTween.scale(button, new Vector3(1, 1, 1), 0.1f).setOnComplete(
                   () =>
                        {
                            onClick();
                        }
               );
                    }
                );
        }
    }
}

