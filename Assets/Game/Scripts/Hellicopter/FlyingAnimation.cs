﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARTest.Game.Helicopter
{
    public class FlyingAnimation : MonoBehaviour
    {
        [SerializeField]
        private GameObject avatar1;
        [SerializeField]
        private GameObject m_Propeller;

        private bool isPropellerLoop = true;

        void OnEnable()
        {
            HellicopterBehaviour.onObjectFlying += FlyingSequence;
        }

        void OnDisable()
        {
            HellicopterBehaviour.onObjectFlying -= FlyingSequence;
        }

        private void FlyingSequence()
        {
            //Rotate Propeller
            isPropellerLoop = true;
            RotatePropeller();
            //Play Audio
            AudioController.Play("FlySound");

            //Sequence
            var seq = LeanTween.sequence();
            //Depart
            seq.append(LeanTween.moveLocalY(avatar1, 2f, 1f).setEase(LeanTweenType.linear));
            //Move foward
            seq.append(LeanTween.moveLocalZ(avatar1, -2f, 1f).setEase(LeanTweenType.linear));
            seq.append(LeanTween.rotateAround(avatar1, Vector3.up, -120, 1f).setEase(LeanTweenType.linear));
            //Move Left
            seq.append(LeanTween.moveLocal(avatar1, new Vector3(2f, 2f, 0), 1f).setEase(LeanTweenType.linear));
            seq.append(LeanTween.rotateAround(avatar1, Vector3.up, -90, 1f).setEase(LeanTweenType.linear));
            //Move behind
            seq.append(LeanTween.moveLocal(avatar1, new Vector3(0, 2f, 2f), 1f).setEase(LeanTweenType.linear));
            seq.append(LeanTween.rotateAround(avatar1, Vector3.up, -90, 1f).setEase(LeanTweenType.linear));
            //Move Right
            seq.append(LeanTween.moveLocal(avatar1, new Vector3(-2f, 2f, 0), 1f).setEase(LeanTweenType.linear));
            seq.append(LeanTween.rotateAround(avatar1, Vector3.up, -60, 1f).setEase(LeanTweenType.linear));
            //Back to center
            seq.append(LeanTween.moveLocal(avatar1, new Vector3(0, 2f, 0), 1f).setEase(LeanTweenType.linear));
            //Arrive
            seq.append(LeanTween.moveLocalY(avatar1, 0.123f, 2f).setEase(LeanTweenType.linear).setOnComplete(() =>
            {
                isPropellerLoop = false;
                AudioController.Stop("FlySound");
            }));

        }

        private void RotatePropeller()
        {
            LeanTween.rotateAroundLocal(m_Propeller, Vector3.up, 360, 0.7f).setOnComplete(
                () =>
                {
                    if (isPropellerLoop)
                        RotatePropeller();
                }
            );
        }
    }
}
