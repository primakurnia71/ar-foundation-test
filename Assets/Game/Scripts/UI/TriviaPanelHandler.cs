﻿using UnityEngine;
using ARTest.Game.Helicopter;

namespace ARTest.Game.UI
{
    public class TriviaPanelHandler : MonoBehaviour
    {
        [SerializeField]
        private GameObject triviaPanel;

        private bool m_isPanelActive = false;
        private void OnEnable()
        {
            HellicopterBehaviour.onObjectClicked += ShowPanel;
        }

        private void OnDisable()
        {
            HellicopterBehaviour.onObjectClicked -= ShowPanel;
        }

        void ShowPanel()
        {
            m_isPanelActive = !m_isPanelActive;
            PanelAnimation(m_isPanelActive);
        }

        void PanelAnimation(bool active)
        {
            if (active)
            {
                triviaPanel.SetActive(m_isPanelActive);
                LeanTween.scaleX(triviaPanel, 10, 0.5f);
            }
            else
            {
                LeanTween.scaleX(triviaPanel, 1, 0.5f).setOnComplete(() => { triviaPanel.SetActive(m_isPanelActive); });
            }
        }
    }
}

