﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

namespace ARTest.Game.UI
{
    public class ButtonHandler : MonoBehaviour
    {
        [SerializeField]
        private string _sceneTarget;
        [Header("UI Button")]
        [SerializeField]
        private Button _btnQuit = null;
        [SerializeField]
        private Button _btnPlay = null;
        [SerializeField]
        private LevelLoader _levelLoader = null;
        void Start()
        {
            _btnPlay?.onClick.AddListener(OnButtonPlay);
            _btnQuit?.onClick.AddListener(OnButtonQuit);
        }

        private void OnButtonPlay(){
            AudioController.Play("Click");
            LeanTween.scale(_btnPlay.gameObject,new Vector3(1.1f,1.1f,1.1f),0.1f).setOnComplete(
                ()=>{LeanTween.scale(_btnPlay.gameObject,new Vector3(1,1,1),0.1f).setOnComplete(
                    ()=>{_levelLoader.LoadLevelIndex(_sceneTarget);
                    }
                );}
            );          
        }

        private void OnButtonQuit(){
            Debug.Log("Quit App");
            AudioController.Play("Click");
            LeanTween.scale(_btnQuit.gameObject,new Vector3(1.1f,1.1f,1.1f),0.1f).setOnComplete(
                ()=>{LeanTween.scale(_btnQuit.gameObject,new Vector3(1,1,1),0.1f).setOnComplete(
                    ()=>
                    {            
                        Application.Quit();
                    }
                );}
            );
        }
    }
}

