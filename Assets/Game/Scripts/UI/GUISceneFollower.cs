﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARTest.Game.UI
{
    public class GUISceneFollower : MonoBehaviour
    {
        private Transform target;
        public float speed = 1.0f;

        private void Start()
        {
            target = Camera.main.transform;
        }

        void Update()
        {
            Vector3 targetDirection = target.position - transform.position;
            float singleStep = speed * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
            Debug.DrawRay(transform.position, newDirection, Color.red);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }
    }
}
