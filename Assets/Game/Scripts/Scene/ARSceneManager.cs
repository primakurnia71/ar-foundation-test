﻿using UnityEngine;

namespace ARTest.Game.Scene
{
    public class ARSceneManager : MonoBehaviour
    {
        [SerializeField]
        private LevelLoader _levelLoader = null;
        [SerializeField]
        private string _sceneTarget;

        // Update is called once per frame
        void Update()
        {
            CheckIfBackPressed();
        }

        void CheckIfBackPressed()
        {
            if (Application.platform == RuntimePlatform.Android)
            {

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    _levelLoader.LoadLevelIndex(_sceneTarget);
                }
            }
        }
    }
}

