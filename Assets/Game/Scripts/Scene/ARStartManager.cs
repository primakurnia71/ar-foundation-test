﻿using UnityEngine;

namespace ARTest.Game.Scene
{
    public class ARStartManager : MonoBehaviour
    {
        public GameObject titleTex;
        public GameObject buttonPlay;

        private bool isReadyToQuit = false;
        // Start is called before the first frame update
        void Start()
        {
            AnimateUI();
        }

        // Update is called once per frame
        void Update()
        {
            CheckIfBackPressed();
        }

        void AnimateUI()
        {
            LeanTween.moveLocalX(titleTex, 0, 1f);
            LeanTween.moveLocalX(buttonPlay, 0, 1f);
        }

        void CheckIfBackPressed()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (isReadyToQuit)
                    {
                        Application.Quit();
                    }
                    else
                    {
#if UNITY_ANDROID
                        ShowToast();
#endif
                    }
                    isReadyToQuit = !isReadyToQuit;
                }
            }
        }

        private void ShowToast()
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            object[] toastParams = new object[3];

            //create a class reference of unity player activity
            AndroidJavaClass unityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

            //set the first object in the array 
            //as current activity reference
            toastParams[0] = unityActivity.GetStatic<AndroidJavaObject>("currentActivity");

            //set the second object in the array 
            //as the CharSequence to be displayed
            toastParams[1] = "Press back again to Quit";

            //set the third object in the array
            //as the duration of the toast from
            toastParams[2] = toastClass.GetStatic<int>("LENGTH_LONG");
            //call static function of Toast class, makeText
            AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", toastParams);
            toastObject.Call("show");
        }
    }
}
