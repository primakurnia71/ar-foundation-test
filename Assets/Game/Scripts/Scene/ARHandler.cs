﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;

public class ARHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject m_PlacedPrefab = null;

    private ARRaycastManager m_RaycastManager;

    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    public GameObject spawnedObject { get; private set; }

    public static event Action onPlacedObject;

    void Start()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
    }

    // Update is called once per frame
    void Update()
    {
        TouchToSpawnObject();
    }

    void TouchToSpawnObject()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                if (m_RaycastManager.Raycast(touch.position, hits, TrackableType.PlaneWithinPolygon))
                {
                    Pose hitPose = hits[0].pose;
                    if (!spawnedObject)
                    {
                        spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, m_PlacedPrefab.transform.rotation);

                        if (onPlacedObject != null)
                        {
                            onPlacedObject();
                        }
                    }
                }
            }
        }
    }
}
